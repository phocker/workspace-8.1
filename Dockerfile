FROM registry.gitlab.com/phocker/php-8.1
LABEL maintainer="Justin Seliga <gitlab@emails.justin.seliga.me>"
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y

# Install MySQL
RUN apt-get install -y mysql-client

# Install PostgreSQL
RUN apt-get install -y postgresql-client

# Install SMB
RUN apt-get install -y smbclient

# Install SOAP
RUN apt-get install -y libxml2-dev

# Install SSH
RUN apt-get install -y libssh2-1-dev

# Install Extensible Stylesheet Language
RUN apt-get install libxslt-dev

# Install composer
RUN apt-get install -y composer

# Install node
RUN apt-get install -y nodejs
RUN apt-get install -y npm

# Install yarn
RUN apt-get install -y yarn
